# Unity plant movement example project 

This project is intended to show several plant movement techniques.
They are organized by usage intention and cost.

The currently implemented techniques are:
 * Cheap shader-based vertex pushing by distance. Usage: small or far plants. Cost: very cheap.

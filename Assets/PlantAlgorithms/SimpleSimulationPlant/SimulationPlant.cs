using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace PlantAlgorithms.SimpleSimulationPlant
{
    public class SimulationPlant : SerializedMonoBehaviour
    {
        public List<Transform> bones = new();

        public List<int> fixedBones = new();
        
        public List<float> stiffness = new();

        private readonly List<float> _boneSizes = new();
        
        private readonly List<Quaternion> _boneOriginalRotations = new();
        
        private readonly List<Vector3> _boneOriginalUpVectors = new();
        
        private readonly List<Vector3> _startingBonePositions = new();

        public int simulationIterationsPerFrame = 1;
        public int constraintSolvingIterationsPerFrame = 1;

        private readonly List<Vector3> _boneVelocities = new();

        public bool useGravity;
        public Vector3 gravity;

        public bool useSpringForceToOriginalPositions;
        public float springKConstant = 0.1f;
        
        private float SubStepDeltaTime => Time.deltaTime / simulationIterationsPerFrame;

        private const float MinSpringUpdateSqDistance = 0.009f;

        public List<Transform> pushers = new();
        public float pushMaxDistance;
        
        private void OnDrawGizmosSelected()
        {
            // Draw a yellow sphere at the transform's position
            Gizmos.color = Color.yellow;

            for (int i = 0; i < bones.Count; i++)
            {
                if (i < bones.Count - 1)
                {
                    Gizmos.DrawLine(bones[i].position, bones[i+1].position);
                }
                
                Gizmos.DrawSphere(bones[i].position, 0.01f);
            }
        }
        
        private void Awake()
        {
            CacheBoneSizes();
            InitializeBoneMetadata();
        }

        private void InitializeBoneMetadata()
        {
            _boneOriginalRotations.Clear();
            var mainParent = bones[0].parent;
            foreach (Transform bone in bones)
            {
                _boneVelocities.Add(Vector3.zero);
                _startingBonePositions.Add(bone.position);
                _boneOriginalUpVectors.Add(bone.up);
                _boneOriginalRotations.Add(bone.rotation);
                
                foreach (Transform child in bone)
                {
                    child.parent = mainParent;
                }
            }
            
            /*
            for (int i = 0; i < bones.Count - 1; i++)
            {
                _boneOriginalDirections.Add((bones[i+1].position - bones[i].position).normalized);
            }
            _boneOriginalDirections.Insert(0, _boneOriginalDirections[1]);
            */
        }

        private void Update()
        {
            for (int i = 0; i < simulationIterationsPerFrame; i++)
            {
                Simulate();
            }
        }

        private void Simulate()
        {
            if (useGravity)
            {
                ApplyGravity();
            }

            if (useSpringForceToOriginalPositions)
            {
                ApplySpringForceToOriginalBonePositions();
            }

            ApplyBoneVelocities();

            for (int i = 0; i < constraintSolvingIterationsPerFrame; i++)
            {
                KeepDistanceConstraints();
            }
            
            ApplyPusherForces();

            ApplyBoneRotation();
        }

        private void ApplyBoneRotation()
        {
            for (var i = 0; i < bones.Count; i++)
            {
                if (IsBoneFixed(i))
                {
                    continue;
                }

                Vector3 bonePosition = bones[i].position;
                var boneOriginalRotation = _boneOriginalRotations[i];
                var boneOriginalDirection = boneOriginalRotation * Vector3.up;
                var boneTranslationDirection = (bonePosition - _startingBonePositions[i]).normalized;
                boneTranslationDirection.y = 0;
                var artificialLookVector = Vector3.Lerp(boneOriginalDirection, boneTranslationDirection, 0.6f);
                
                Debug.DrawLine(bonePosition, bonePosition+boneOriginalDirection*0.5f, Color.blue);
                Debug.DrawLine(bonePosition, bonePosition+boneTranslationDirection*0.5f, Color.red);
                Debug.DrawLine(bonePosition, bonePosition+artificialLookVector, Color.green);
                Debug.DrawLine(bonePosition, bonePosition+_boneOriginalUpVectors[i]*0.5f, Color.magenta);
                
                var finalDirection = Quaternion.Inverse(boneOriginalRotation) * artificialLookVector;
                var finalLookRotation = boneOriginalRotation * Quaternion.FromToRotation(Vector3.up, finalDirection);

                var stiffLookRotation = Quaternion.Lerp(finalLookRotation, boneOriginalRotation, stiffness[i]);
                
                //bones[i].rotation = Quaternion.LookRotation(-artificialLookVector, boneOriginalDirection);
                bones[i].rotation = stiffLookRotation;
                //bones[i].up = artificialLookVector;
                //bones[i].forward = -boneTranslationDirection;
            }
        }
        
        private void ApplyGravity()
        {
            for (var index = 0; index < _boneVelocities.Count; index++)
            {
                if (IsBoneFixed(index))
                {
                    continue;
                }

                _boneVelocities[index] += gravity * SubStepDeltaTime;
            }
        }

        private void ApplyBoneVelocities()
        {
            for (var index = 0; index < _boneVelocities.Count; index++)
            {
                if (IsBoneFixed(index))
                {
                    continue;
                }

                Vector3 boneVelocity = _boneVelocities[index];
                bones[index].position += boneVelocity;// * (SubStepDeltaTime * 100f);
                _boneVelocities[index] = Vector3.zero;
            }
        }
        
        private void ApplyPusherForces()
        {
            foreach (Transform pusher in pushers)
            {
                UpdatePusherForce(pusher.position);
            }
        }

        private void UpdatePusherForce(Vector3 pusherPosition)
        {
            for (int i = 0; i < bones.Count; i++)
            {
                if (IsBoneFixed(i))
                {
                    continue;
                }

                Vector3 currentBonePosition = bones[i].position;
                Vector3 fromPusherToBone = currentBonePosition - pusherPosition;

                float distance = fromPusherToBone.magnitude;

                if (distance > pushMaxDistance)
                {
                    continue;
                }

                Vector3 normalizedPusherToBone = fromPusherToBone/distance;
                bones[i].position += normalizedPusherToBone * (pushMaxDistance - distance);
            }
        }

        private void ApplySpringForceToOriginalBonePositions()
        {
            for (int i = 0; i < bones.Count; i++)
            {
                if (IsBoneFixed(i))
                {
                    continue;
                }
                
                Vector3 currentBonePosition = bones[i].position;
                Vector3 startingBonePosition = _startingBonePositions[i];
                Vector3 fromBoneToOriginalPosition = startingBonePosition - currentBonePosition;
                
                float sqDistance = fromBoneToOriginalPosition.sqrMagnitude;
                if (sqDistance < MinSpringUpdateSqDistance)
                {
                    continue;
                }
                
                float distance = Mathf.Sqrt(sqDistance);
                Vector3 dirToOriginalPos = fromBoneToOriginalPosition / distance;

                float springForce = springKConstant * distance;
                _boneVelocities[i] += dirToOriginalPos * (springForce * SubStepDeltaTime);
            }
        }

        private void CacheBoneSizes()
        {
            _boneSizes.Clear();
            if (bones.Count <= 1)
            {
                return;
            }
            
            for (int i = 0; i < bones.Count-1; i++)
            {
                Vector3 currentBonePosition = bones[i].position;
                Vector3 nextBonePosition = bones[i + 1].position;
                float distance = Vector3.Distance(currentBonePosition, nextBonePosition);
                _boneSizes.Add(distance);
            }
        }

        private void KeepDistanceConstraints()
        {
            if (bones.Count <= 1)
            {
                return;
            }
            
            for (int i = 0; i < bones.Count-1; i++)
            {
                Transform currentBone = bones[i];
                Transform nextBone = bones[i + 1];
                
                Vector3 currentBonePosition = currentBone.position;
                Vector3 nextBonePosition = nextBone.position;

                float actualDistance = Vector3.Distance(currentBonePosition, nextBonePosition);
                float correctDistance = _boneSizes[i];
                float delta = actualDistance - correctDistance;
                
                //if actual distance is greater then delta is positive
                
                if (delta <=  0) continue;
                
                bool isCurrentFixed = IsBoneFixed(i);
                KeepDistanceConstraintBetweenBonesByPosition(nextBonePosition, currentBonePosition, isCurrentFixed, nextBone, delta, currentBone);
            }
        }

        private bool IsBoneFixed(int i)
        {
            return fixedBones.Contains(i);
        }

        private static void KeepDistanceConstraintBetweenBonesByPosition(Vector3 nextBonePosition, Vector3 currentBonePosition, bool isCurrentFixed,
            Transform nextBone, float delta, Transform currentBone)
        {
            Vector3 currentBoneToNext = (nextBonePosition - currentBonePosition).normalized;
            Vector3 nextBoneToCurrent = -1 * currentBoneToNext;
            
            if (isCurrentFixed)
            {
                nextBone.position = nextBonePosition + nextBoneToCurrent * delta;
            }
            else
            {
                float halfDelta = delta * 0.5f;
                currentBone.position = currentBonePosition + currentBoneToNext * halfDelta;
                nextBone.position = nextBonePosition + nextBoneToCurrent * halfDelta;
            }
        }
    }
}

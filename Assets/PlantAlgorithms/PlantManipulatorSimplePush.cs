using UnityEngine;

namespace PlantAlgorithms
{
    [ExecuteInEditMode]
    public class PlantManipulatorSimplePush : MonoBehaviour
    {
        public new Renderer renderer;
        
        public Transform point;
        public float yCurveIntensity = 4;
        public float defectIntensity = 0.1f;
        
        public float minH;
        public float maxH;

        public float pushRange = 1f;
        
        private MaterialPropertyBlock _materialPropertyBlock;
        private static readonly int pushPoint = Shader.PropertyToID("_PushPoint");
        private static readonly int curveIntensity = Shader.PropertyToID("_yCurveIntensity");
        private static readonly int effectIntensity = Shader.PropertyToID("_effectIntensity");
        private static readonly int minHeight = Shader.PropertyToID("_minHeight");
        private static readonly int maxHeight = Shader.PropertyToID("_maxHeight");
        private static readonly int attenuation = Shader.PropertyToID("_attenuation");

        private MaterialPropertyBlock Mpb => _materialPropertyBlock ??= new MaterialPropertyBlock();

        private void Start()
        {
            renderer.GetPropertyBlock(Mpb);
        }
        
        private void Update()
        {
            if(!point) return;
            
            float attenuationValue = 1.0f / Mathf.Max(0.0001f, pushRange * pushRange);
            
            renderer.GetPropertyBlock(Mpb);
            Mpb.SetVector(pushPoint, point.position);
            Mpb.SetFloat(curveIntensity, yCurveIntensity);
            Mpb.SetFloat(effectIntensity, defectIntensity);
            Mpb.SetFloat(minHeight, minH);
            Mpb.SetFloat(maxHeight, maxH);
            Mpb.SetFloat(attenuation, attenuationValue);
            renderer.SetPropertyBlock(Mpb);
        }
    }
}
